<?php

namespace Score\BlockBundle\Controller;

use Symfony\Component\Form\Form;
use Score\BlockBundle\Entity\Block;
use Score\BlockBundle\Form\BlockType;
use Score\CmsBundle\Entity\ChangesLog;
use Score\BlockBundle\Form\FullBlockType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Score\BlockBundle\Services\CmsBlockManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Block controller.
 * @Route("/admin/block", name="score-block", methods={"GET", "POST"})
 */
class AdminController extends AbstractController
{

    /**
     * Lists all Block entities.
     * @Route("/show", name="-show", methods={"GET"})
     */
    public function indexAction(CmsBlockManager $manager)
    {
        //$manager = $this->get('score.cms.block.manager');
        $groupEnum = $manager->getGroups();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository(Block::class)->loadBlocksList();
//        $groups = array();
//        foreach($entities as $block)
//        {
//            if($block->getType() != 'pageContent')
//            {
//                $groups[$block->getType()]['blocks'][$block->getSortOrder()] = $block;
//                $groups[$block->getType()]['name'] = $groupEnum[$block->getType()];
//                ksort($groups[$block->getType()]['blocks']);
//            }
//
//        }

        return $this->render('@ScoreBlock/Admin/index.html.twig', [
//            'groups' => $groups,
            'blocks' => $entities,
        ]);

    }

    /**
     * Creates a new Block entity.
     * @Route("/new", name="-new", methods={"GET", "POST"})
     */
    public function createAction(CmsBlockManager $manager, Request $request)
    {
        $groupEnum = $manager->getGroups();

        $entity = new Block();
        $form = $this->createForm(BlockType::class, $entity, ['groups' => array_flip($groupEnum)]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entity->setSortOrder(time());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('score-block-edit', array('id' => $entity->getId())));
        }

        return $this->render('@ScoreBlock/Admin/edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'logs' => []
        ));
    }


    /**
     * Displays a form to edit an existing Block entity.
     * @Route("/edit/{id}", name="-edit", methods={"GET", "POST"})
     */
    public function editAction(CmsBlockManager $manager, Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $block = $em->getRepository(Block::class)->find($id);

        if (!$block) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }
        $groupEnum = $manager->getGroups();

        $editForm = $this->createForm(BlockType::class, $block, ['groups' => array_flip($groupEnum)]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            //return new Response('<html>ok</html>');

            return $this->redirectToRoute('score-block-edit', array('id' => $block->getId()));
        }

        $logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Block::class, "entityId" => $id, "field" => "content"], ["editedAt" => "DESC"],15);

        return $this->render('@ScoreBlock/Admin/edit.html.twig', [
            'entity' => $block,
            'form' => $editForm->createView(),
            'logs' => $logs
        ]);
    }

    /**
     * Deletes a Block entity.
     * @Route("/delete/{id}", name="-delete", methods={"GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository(Block::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('score-block-show'));
    }

    /**
     * @Route("/edit-expert/{id}", name="-edit-expert", methods={"GET", "POST"})
     */
    public function expertEditAction(CmsBlockManager $manager, Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $block = $em->getRepository(Block::class)->find($id);

        if (!$block) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }

        $editForm = $this->createForm(FullBlockType::class, $block);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            //return new Response('<html>ok</html>');

            return $this->redirectToRoute('score-block-edit-expert', array('id' => $block->getId()));
        }

        $logs = $em->getRepository(ChangesLog::class)->findBy(["entity" => Block::class, "entityId" => $id, "field" => "content"], ["editedAt" => "DESC"],15);

        return $this->render('@ScoreBlock/Admin/edit_expert.html.twig', [
            'entity' => $block,
            'form' => $editForm->createView(),
            'logs' => $logs
        ]);
    }

}
