<?php

namespace Score\BlockBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function indexAction($name)
    {
        return $this->render('ScoreBlockBundle:Default:index.html.twig', array('name' => $name));
    }
}
