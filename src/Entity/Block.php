<?php

namespace Score\BlockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Score\BaseBundle\Entity\BaseEntity;
use Score\BlockBundle\Repository\BlockRepository;

/**
 * Block
 * @ORM\Entity(repositoryClass=BlockRepository::class)
 * @ORM\Table(name="cms_block")
 * @ORM\HasLifecycleCallbacks()

 */
class Block extends BaseEntity {


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="perex", type="text", nullable=true)
     */
    private $perex;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=50, nullable=true)
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=200, nullable=true)
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=200, nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", nullable=true)
     */
    private $params;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=200, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;



    /**
     * @ORM\OneToMany(targetEntity="Score\PageBundle\Entity\PageBlock", mappedBy="block")
     */
    private $pageBlocks;


    /**
     * Set name
     *
     * @param string $name
     * @return Block
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set perex
     *
     * @param string $perex
     * @return Block
     */
    public function setPerex($perex)
    {
        $this->perex = $perex;

        return $this;
    }

    /**
     * Get perex
     *
     * @return string
     */
    public function getPerex()
    {
        return $this->perex;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Block
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return Block
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Block
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Block
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return Block
     */
    public function setParams($params)
    {
        $this->params = json_encode($params);

        return $this;
    }

    /**
     * Get params
     *
     * @return string
     */
    public function getParams()
    {
        return json_decode($this->params, true);
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Block
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return Block
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pageBlocks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getPageBlocks()
    {
        return $this->pageBlocks;
    }

    function setPageBlocks($pageBlocks)
    {
        $this->pageBlocks = $pageBlocks;
    }

    public function getMenuId()
    {
        $params = $this->getParams();
        $settings = json_decode($params,true);
       return $settings['menuId'];
    }

}
