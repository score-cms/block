<?php

namespace Score\BlockBundle\Form;

use Score\BlockBundle\Entity\Block;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

//use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FullBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'score.block.edit.name'])
            ->add('lang', null, ['label' => 'score.block.edit.lang'])
            ->add('module', null, ['label' => 'score.block.edit.module'])
            ->add('action', null, ['label' => 'score.block.edit.action'])
            ->add('params', null, ['label' => 'score.block.edit.params'])
            ->add('type', null, ['label' => 'score.block.edit.type'])
            ;
           
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Block::class,
            'groups' => []
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'score_block';
    }
}
