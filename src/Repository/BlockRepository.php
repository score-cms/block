<?php

namespace Score\BlockBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BlockRepository extends EntityRepository
{
    public function loadBlocksList()
    {
        return $this->createQueryBuilder('b')
            ->where('b.type != :nt')
            ->setParameter('nt', "pageContent")
            ->orderBy('b.editedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getCount()
    {
        $qb = $this->createQueryBuilder('b');
        $qb->select('count(b.id)');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }
}
