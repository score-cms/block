<?php


namespace Score\BlockBundle\Services;

use Score\BaseBundle\Services\BaseManager;


class CmsBlockManager extends BaseManager
{
    public function getGroups()
    {
        $groups = [
            'static' => 'Statický text',
            'menu' => 'Menu',
            'news' => 'Novinka',
        ];
        return $groups;
    }

    public function getBlockById($id)
    {
        return $this->getRepository()->find($id);
    }

    public function reorderBlocks($sortList)
    {
        $repo = $this->getRepository();
        $em = $this->getDbProvider()->getManager();
        foreach ($sortList as $sort => $id) {
            $sort++;
            $block = $repo->findOneBy(array('id' => $id));
            $block->setSortOrder($sort);
            $em->persist($block);
        }
        $em->flush();

    }

 
}


